<?php
   class gana_budget_Line {
      private $id;
      private $Valor;
      private $Nombre;
      private $Dependencia;
      private $imagen;
      //private $number;

      public function  __construct($id=NULL, $Valor=NULL, $Nombre=NULL, $Dependencia=NULL, $imagen=NULL) {        
        $this->id = $id;
        $this->Valor = $Valor;
        $this->Nombre = $Nombre;
        $this->Dependencia = $Dependencia;
        $this->imagen = $imagen;
      }

      public function get_id(){
        return $this->id;
      }
      public function set_id($id){
        $this->id = $id;
      } 
      
      public function get_Valor(){
        return $this->Valor;
      }
      public function set_Valor($Valor){
        $this->Valor = $Valor;
      }
      
      public function get_Nombre(){
        return $this->Nombre;
      }
      public function set_Nombre($Nombre){
        $this->Nombre = $Nombre;
      }

      public function get_Dependencia(){
        return $this->Dependencia;
      }
      public function set_Dependencia($Dependencia){
        $this->Dependencia = $Dependencia;       
      }

      public function get_imagen(){
        return $this->imagen;
      }
      public function set_imagen($imagen){
        $this->imagen = $imagen;
      }
      public function toArray() {
        return $this->processArray(get_object_vars($this));
      }
      
      private function processArray($array) {
        foreach($array as $key => $value) {
          if (is_object($value)) {
            $array[$key] = $value->toArray();
          }
          if (is_array($value)) {
            $array[$key] = $this->processArray($value);
          }
        }        
        return $array;
      }
       
      public function __toString() {
        return json_encode($this->toArray(), JSON_UNESCAPED_UNICODE);
      }

    }
?>
