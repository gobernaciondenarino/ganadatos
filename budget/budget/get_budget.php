<?php
	include('classes/gana_budget_Line.php');
	include('includes/simple_html_dom.php');
	
	if (isset($_GET['type']) && isset($_GET['cat']) && isset($_GET['year'])) {
		$p_type = $_GET['type'];
		$p_cat = $_GET['cat'];
		$p_year = $_GET['year'];
		if($p_type !=null && $p_cat != null && $p_year !=null) {
			error_log("Consultando datos por [type: ".$p_type."; cat: ".$p_cat."; year: ".$p_year."]", 0);		
			getBudgetJson($p_type, $p_cat, $p_year);			
		} else
			echo "Error 2. Los parámetros no pueden ser vacíos.";
	} else
		echo "Error 1. Los parámetros no son válidos. Verifique haber enviado el número y nombre de parámetros apropiado.";


	//if( !cURLcheckBasicFunctions() ) return "UNAVAILABLE: cURL Basic Functions"; 

	function getBudgetJson($type, $category, $year){
		$budget_exists = true;

		$vig = trim($year);

		$url = 'http://aplicaciones.narino.gov.co/SCP/xml/cuenta_'.$vig.'.xml';

		$valor_tag = "";
		$nombre_tag = "";

		switch ($type) {
			case 'com':
				$valor_tag = "comprometido";
				break;
			case 'dev':
				$valor_tag = "devengado";
				break;
			case 'pag':
				$valor_tag = "pagado";
				break;							
		}

		switch ($category) {
			case 'og':
				$nombre_tag = "clasificador_economico_desc_3_digitos";
				break;
			case 'ffin':
				$nombre_tag = "fuente_financiamiento_desc";
				break;
			case 'fin':
				$nombre_tag = "finalidad_funcion_desc_finalidad";
				break;							
		}

		$cURL = curl_init();
		curl_setopt($cURL, CURLOPT_URL, $url);
		curl_setopt($cURL, CURLOPT_RETURNTRANSFER, 1);		
		$result = curl_exec($cURL);
		curl_close($cURL);
		$dom = new simple_html_dom();
	    $dom->load($result);

	    $records = $dom->find('registro');
	    $num_records = count($records);
	    $count_records = 0;

	    error_log("serving ".$num_records." records");

	    header('Content-Type: application/json; charset=utf-8');
	    echo "[";
	    $img = "img/icon/icon-reloj.png";

	    foreach ($records as $record){	    

	    	$line = new gana_budget_Line();	
	    	
			$dep = $record->getElementByTagName('institucion_desc_entidad');
	    	$dependencia = $dep->innertext;	
			$line->set_Dependencia($dependencia);

		    $nom = $record->getElementByTagName($nombre_tag);
	    	$nombre = $nom->innertext;
			$line->set_Nombre($nombre);

			$val = $record->getElementByTagName($valor_tag);
			$valor = $val->innertext;
			$line->set_Valor($valor);
			
			$line->set_imagen($img);

		    $id = $record->getAttribute('dato');
	    	$line->set_id($id);

			echo $line;

			if(++$count_records < $num_records)
				echo ",";
			
	    }
	    echo "]";
	}


	function cURLcheckBasicFunctions(){
		if(!function_exists("curl_init") &&
		!function_exists("curl_setopt") &&
		!function_exists("curl_exec") &&
		!function_exists("curl_close") ) 
			return false;
		else 
			return true;
	} 
?>

