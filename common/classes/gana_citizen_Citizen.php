<?php
   class gana_citizen_Citizen {
      private $id;
      private $state;
      private $city;
      private $address;
      private $spot;
      private $number;

      public function  __construct($id, $state, $city, $address, $spot, $number) {        
        $this->id = $id;
        $this->state = $state;
        $this->city = $city;
        $this->address = $address;
        $this->spot = $spot;
        $this->number = $number;
      }

      public function get_id(){
        return $this->id;
      }
      public function set_id($id){
        $this->id = $id;
      } 
      
      public function get_state(){
        return $this->state;
      }
      public function set_state($state){
        $this->state = $state;
      }
      
      public function get_city(){
        return $this->city;
      }
      public function set_city($city){
        $this->city = $city;
      }

      public function get_address(){
        return $this->address;
      }
      public function set_address($address){
        $this->address = $address;       
      }

      public function get_spot(){
        return $this->spot;
      }
      public function set_spot($spot){
        $this->spot = $spot;
      }

      public function get_number(){
        return $this->number;
      }
      public function set_number($number){
        $this->number = $number;
      }

      public function toArray() {
        return $this->processArray(get_object_vars($this));
      }
      
      private function processArray($array) {
        foreach($array as $key => $value) {
          if (is_object($value)) {
            $array[$key] = $value->toArray();
          }
          if (is_array($value)) {
            $array[$key] = $this->processArray($value);
          }
        }        
        return $array;
      }
       
      public function __toString() {
        //return json_encode(array_map('utf8_encode', $this->toArray()));
        return json_encode($this->toArray(), JSON_UNESCAPED_UNICODE);
      }

    }
?>
