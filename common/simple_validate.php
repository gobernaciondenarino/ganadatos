<?php
header('Content-Type: application/json; charset=utf-8');
	include('includes/simple_html_dom.php');
	include('classes/gana_citizen_Citizen.php');

	$citizen = new gana_citizen_Citizen();

	if (isset($_GET['id'])) {
		$p_id = $_GET['id'];	
		$citizen = checkRemoteCensus($p_id);
	} 
	echo $citizen;
		
	function checkRemoteCensus($id){

		$citizen = new gana_citizen_Citizen();
		$citizen_exists = true;
		
		$not_found_string = "<strong>No ";

		$data = array('Nuip' => $id);                      
		$url = 'http://registraduria.org/ccenso.php';
		$cURL = curl_init();
		curl_setopt($cURL, CURLOPT_URL, $url);
		curl_setopt($cURL, CURLOPT_POST, 1);
		curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($cURL, CURLOPT_POSTFIELDS, http_build_query($data));
		curl_setopt($cURL, CURLOPT_HTTPHEADER, array(
		    'Host: registraduria.org',
		    'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:41.0) Gecko/20100101 Firefox/41.0',
		    'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
		    'Accept-Language: en-US,en;q=0.5',
		    'Content-Type: application/x-www-form-urlencoded; charset=UTF-8',
		    'Referer: http://registraduria.org/censo.php',
		    'Connection: keep-alive',
		    'Pragma: no-cache',
		    'Cache-Control: no-cache'
		));
		
		$result = curl_exec($cURL);
		curl_close($cURL);
		
		$dom = new simple_html_dom();
	    $dom->load($result);
	    
	    $state = NULL;
	    $city = NULL;
	    
	    foreach ($dom->find('div#ttlo') as $e)
	    	if(substr($e->innertext, 0, strlen($not_found_string)) === $not_found_string) {
	    		$id = 	NULL;
	    		$citizen_exists = false;
	    	}
	    
	    if($citizen_exists) {
			$count = 0;
		    foreach($dom->find('td.tblbgcolort') as $e){
		    	if($count==1)
		    		$state = $e->innertext;
		    	$count++;
			}

			$count = 0;
		    foreach($dom->find('tr.tblbgcolort') as $e){
		    	if($count==0)
		    		$spot = $e->find('td', 1)->innertext;
		    	$count++;
			}

			$count = 0;
		    foreach($dom->find('td.tblbgcolor') as $e){
		    	if($count==1)
		    		$city = $e->innertext;
				if($count==3) {
					$address = $e->find('div', 0)->innertext;
				}		    		
				if($count==5)
		    		$number = $e->innertext;
		    	$count++;
			}	    	
	    }
		$citizen->set_id($id);
		$citizen->set_state($state);
		$citizen->set_city($city);
		$citizen->set_spot($spot);
		$citizen->set_address($address);
		$citizen->set_number($number);

		return $citizen;
	}
?>
