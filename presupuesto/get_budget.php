<?php
	header('Content-Type: application/json; charset=utf-8');
	$budget = '';
	if (isset($_GET['cat'])) {
		$p_cat = $_GET['cat'];
		error_log("Consultando datos por: ".$p_cat, 0);		
		$budget = getBudgetJson($p_cat);
	} 
	//echo $budget;

	function getBudgetJson($cat){
		$budget_exists = true;
		$not_found_string = "<strong>No ";
		$url = 'http://aplicaciones.narino.gov.co/SCP/xml/cuenta.xml';
		$cURL = curl_init();
		curl_setopt($cURL, CURLOPT_URL, $url);
		curl_setopt($cURL, CURLOPT_POST, 1);
		curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);		
		curl_setopt($cURL, CURLOPT_HTTPHEADER, array(
			'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
			'Accept-Encoding: gzip, deflate',
			'Accept-Language: es-MX,es-ES;q=0.9,es;q=0.7,es-AR;q=0.6,es-CL;q=0.4,en-US;q=0.3,en;q=0.1',
			'Connection: keep-alive',
			'DNT: 1',
			'Host: aplicaciones.narino.gov.co',
			'Upgrade-Insecure-Requests: 1',
			'User-Agent: Mozilla/5.0 (Windows NT 6.3; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0'
		));

		$result = curl_exec($cURL);
		curl_close($cURL);
		$xml = simplexml_load_string($result);
		echo $xml;
		//$json = json_encode($xml);
		//return $json;
	}
?>

