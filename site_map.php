<html>
   <head>
      <title>GANAdatos | Datasets publicados</title>
      <link rel="shortcut icon" href="https://datos.narino.gov.co/sites/default/files/favicon_0.png" type="image/png" />
      <style media="screen" type="text/css">
         /* ------------------
          styling for the tables 
            ------------------   */


         body
         {
	         line-height: 1.6em;
         }
         #one-column-emphasis
         {
	         font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
	         font-size: 12px;
	         margin: 45px;
	         width: 95%;
	         text-align: left;
	         border-collapse: collapse;
         }
         #one-column-emphasis th
         {
	         font-size: 14px;
	         font-weight: normal;
	         padding: 12px 15px;
	         color: #2F7500;
         }
         #one-column-emphasis td
         {
	         padding: 10px 15px;
	         color: #444;
	         border-top: 1px solid #999;
         }
         .oce-first
         {
	         background: #d0dafd;
	         border-right: 10px solid transparent;
	         border-left: 10px solid transparent;
         }
         #one-column-emphasis tr:hover td
         {
	         color: #2F7500;
	         background: #F0FFD5;
            border-top: 1px solid #50961f;
            border-bottom: 1px solid #50961f;
         }

         #one-column-emphasis a {
            color: #50961F;
            text-decoration: none;
         }
         #one-column-emphasis a:hover {
            color: #2F7500;
            text-decoration: underline;
         }

         tr.gray {
            background: #F7FFE8;
         }
      </style> 
   </head>
<body>
   <table id="one-column-emphasis" summary="Recursos GANA Datos">
      <thead>
         <tr><th scope="col">Nro.</th><th scope="col">Recurso</th><th scope="col">Archivo</th><th scope="col">Dataset</th></tr>
      </thead>
      <tbody>
<?php
   $num = 1;
   $url = 'https://datos.narino.gov.co/?q=data.json';
   $cURL = curl_init();
   curl_setopt($cURL, CURLOPT_URL, $url);
   curl_setopt($cURL, CURLOPT_HTTPGET, true);
   curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
   curl_setopt($cURL, CURLOPT_HTTPHEADER, array(
      'Content-Type: application/json',
      'Accept: application/json'
   ));
   $result = curl_exec($cURL);
   curl_close($cURL);
   $json = json_decode($result, true); 
   foreach($json['dataset'] as $dataset) {
      foreach($dataset['distribution'] as $resource){
         $tr_style = ($num%2==0) ? "class='gray'" : "";
         echo "<tr ".$tr_style."><td>".$num."</td><td>".$resource['description']."</td><td><a href='".$resource['downloadURL']."'>".$resource['title']."</a></td><td>".$dataset['title']."</td></tr>";
         $num++;
         
      }
   }
?>
         </tbody>
      </table>
   </body>
</html>
